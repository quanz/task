const Utils = function () {
    let self = this;
    self.sort = sortCommenters;
    self.fetchData = fetchData;
    self.sumComments = sumComments;
}

function sortCommenters(targetObj) {
    const sortable = [];
      Object.entries(targetObj).forEach((obj) => {
        sortable.push(obj);
      });
      const sortObj = sortable.sort((a, b) => {
        return b[1] - a[1];
      });
    return sortObj;
}

function fetchData(source, output, ax) {
    source.forEach((id) => {
        output.push(
            ax.get(`/item/${id}.json`)
            .then((res) => {
                return res.data;
            })
            .catch((err) => {
                log(chalk.red(err));
            }));
        });
}

function sumComments(commentItem, commenterSet, commentsCounter) {
  let commenter = null;
        if (commentItem) {
            commenter = commentItem.by;
        }
        if (commenter && commentItem.type === 'comment') {
          if (!commenterSet.has(commenter)) {
            commenterSet.add(commenter);
            commentsCounter[commenter] = 0;
          }
          commentsCounter[commenter] += 1;
        }
}


module.exports = Utils;