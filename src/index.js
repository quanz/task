const axios = require('axios');
const chalk = require('chalk');
const https = require('https');
const Utils = require('./utils');

const agent = new https.Agent({ family: 4 });
const ax = axios.create({
  baseURL: 'https://hacker-news.firebaseio.com/v0',
  httpsAgent: agent
});

const log = console.log;
const utilsInstance = new Utils();

let start = new Date().getTime();

/**
 * Fetch TOP30 stories' ID from '/topstories.json'
 */
const topStoryId = ax.get('/topstories.json')
  .then((res) => {
    return (res.data.slice(0, 30));
  });

/**
 * Fetch top30 story Item data from '/item/{storyID}.json'
 * RETURN commentIds (for the top30 stories)
 * Print out Story Title for the top30 stories
 */
const commentIds = topStoryId.then((storyIds) => {
  const axiosStoryIds = [];
  utilsInstance.fetchData(storyIds, axiosStoryIds, ax);
  return Promise.all(axiosStoryIds)
    .then((stories) => {
      const comments = [];
      stories.forEach((story, index) => {
        if (story) {
          // Print titles of top30 stories
          log(chalk.green(`Story Top${index + 1}: ${story.title}`));
          if (story.kids) {
            story.kids.forEach((commentId) => {
              comments.push(commentId);
            });
          }
        }
      });
      return comments;
    })
    .catch((err) => {
      log(chalk.red(err));
    });
})
  .catch((err) => {
    log(chalk.red(err));
  });

/**
 * Fetch comment Items' data from '/item/{commentID}.json'
 * Print out top10 commenters (name and comments number) for top30 stories 
 */
commentIds.then((commentItems) => {
  const axiosCommentsData = [];

  utilsInstance.fetchData(commentItems, axiosCommentsData, ax);
  Promise.all(axiosCommentsData)
    .then((commentItems) => {
      const commentsCounter = {};
      const commenterSet = new Set();
      commentItems.forEach((commentItem) => {
        utilsInstance.sumComments(commentItem, commenterSet, commentsCounter);
      });

      let sortObj = utilsInstance.sort(commentsCounter);
      //   Print top10 commenters for these top30 stories
      sortObj.slice(0, 10).forEach((user, index) => {
        log(chalk.yellow(`Commenter Top${index + 1}: ${user[0]} with ${user[1]} comments`));
      });
      let end = new Date().getTime();
      log(chalk.red(`running time: ${end - start}ms`));
    })
    .catch((err) => {
      log(chalk.red(err));
    });
})
  .catch((err) => {
    log(chalk.red(err));
  });
